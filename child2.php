<?php
require_once __DIR__ . '/vendor/autoload.php';

use MyApp\AwsMonitoring;

$obj = new AwsMonitoring();
$data = $obj->getRdsData();
echo json_encode(array("rds" => $data));
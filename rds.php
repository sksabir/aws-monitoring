<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <title>AWS Monitoring</title>

    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>

    <link rel="stylesheet" href="css/style.css">
    <?php

    $json = file_get_contents(dirname(__FILE__) . '/config.json');
    $configs = json_decode($json, TRUE);
    ?>
    <script type="text/javascript">
        var SOCKET_URL = "<?php echo $configs['socket_url']; ?>";
    </script>
</head>

<body>

<div style="text-align: center;"><h2>RDS Monitoring</h2></div>

<div id="aws-matrices" class="container">
    <div style="padding: 10px;" class="connection-msg">{{flag}}</div>
    <input v-model="search" class="form-control" placeholder="Filter by columns">

    <table class="table table-striped">
        <thead>
        <tr>
            <th v-repeat="column: columns">
                <a href="#" v-on="click: sortBy(column)" v-class="active: sortKey == column">
                    {{ column | capitalize }}
                </a>
            </th>
        </tr>
        </thead>

        <tbody>
        <tr v-repeat="matrices | filterBy search | orderBy sortKey reverse">

            <td>{{ DBInstanceIdentifier }}</td>
            <td>{{ cpuUtilization }}</td>
            <td>{{ DBInstanceClass }}</td>
            <td>{{ Engine }}</td>
            <td>{{ DBInstanceStatus }}</td>
            <td>{{ AllocatedStorage }}</td>
            <td>{{ AvailabilityZone }}</td>
            <td>{{ aws_account }}</td>
        </tr>
        </tbody>
    </table>

</div>
<script src='https://cdnjs.cloudflare.com/ajax/libs/vue/0.12.16/vue.min.js'></script>
<script  src="js/rds.js"></script>
</body>

</html>

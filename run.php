<?php
require_once __DIR__ . '/vendor/autoload.php';

$json = file_get_contents(dirname(__FILE__) . '/config.json');
$configs = json_decode($json, TRUE);

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use React\Socket\Server as Reactor;
use React\EventLoop\Factory as LoopFactory;
use MyApp\Server;

$loop = LoopFactory::create();
$socket = new Reactor($configs['socket_port'], $loop);
$server = new IoServer(new HttpServer(new WsServer(new Server($loop))), $socket, $loop);
$server->run();
<?php

namespace MyApp;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use GuzzleHttp\Promise\Promise;

class Server implements MessageComponentInterface
{
    protected $clients;
    protected $status = 0;
    public $loop;

    public function __construct(\React\EventLoop\LoopInterface $loop)
    {
        $this->clients = new \SplObjectStorage;
        $this->loop = $loop;

        $this->manageProcesses();

    }

    public function onOpen(ConnectionInterface $conn)
    {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
        $this->status = 1;
    }


    public function onMessage(ConnectionInterface $from, $msg)
    {
        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');
    }

    public function onClose(ConnectionInterface $conn)
    {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->status = 0;
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

    public function manageProcesses()
    {
        $processes = [];
        $processes[] = new \React\ChildProcess\Process('php child.php');
        $processes[] = new \React\ChildProcess\Process('php child2.php');

        $this->loop->addTimer(0.001, function ($timer) use ($processes) {

            $loop = $timer->getLoop();

            foreach ($processes as $process) {
                $process->start($loop);
            }
        });

        $this->loop->addPeriodicTimer(5, function ($timer) use ($processes) {

            foreach ($processes as $process) {
                if (!$process->isRunning()) {
                    $process->start($timer->getLoop());
                }

                $process->stdout->on('data', function ($output) {
                    foreach ($this->clients as $client) {

                        $client->send($output);
                    }
                });
            }
        });
    }
}
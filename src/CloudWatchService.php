<?php

namespace MyApp;

use Aws\CloudWatch\CloudWatchClient;


class CloudWatchService
{
    public $cloudWatchClient;

    public function __construct($args)
    {
        $this->cloudWatchClient = new CloudWatchClient($args);
    }

    public function prepareMatricStatisticPromise($dimensions,$namespace)
    {
        return $this->cloudWatchClient->getMetricStatisticsAsync([
            'Dimensions' => $dimensions,
            'MetricName' => 'CPUUtilization', // REQUIRED
            'Namespace' => $namespace, // REQUIRED
            'Period' => 300, // REQUIRED
            'StartTime' => strtotime('-10 minutes'),
            'EndTime' => strtotime('now'),
            'Statistics' => ['Average']
        ]);
    }

}
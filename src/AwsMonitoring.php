<?php

namespace MyApp;

use GuzzleHttp\Promise;

class AwsMonitoring
{

    protected $emailApi;
    protected $accounts;
    protected $alertReceivers;
    public $cpuThreshold;

    public function __construct()
    {
        $json = file_get_contents(dirname(dirname(__FILE__)) . '/config.json');
        $configs = json_decode($json, TRUE);

        $this->emailApi = $configs['emailApi'];
        $this->accounts = $configs['accounts'];
        $this->manageAlertSettings();
    }

    private function manageAlertSettings()
    {
        $json = file_get_contents('aws-monotoring-settings.json');
        $data = json_decode($json, TRUE);
        if (empty($data)) {
            echo "Please configure alert receivers and cpu threshold";
            die();
        }
        $this->alertReceivers = $data['email_ids'];
        $this->cpuThreshold = $data['cpu_threshold'];
    }

    public function getData()
    {
        if (!empty($this->accounts)) {

            $promises2 = array();

            $instanceIds = array();
            $final_result = array();

            $count=0;

            foreach ($this->accounts as $account) {

                if (!empty($account['regions'])) {

                    $promises = array();

                    foreach ($account['regions'] as $region) {
                        $args = [
                            'region' => $region,
                            'profile' => $account['profile'],
                            'version' => $account['version'],
                        ];
                        $objEc2 = new Ec2Service($args);
                        $promises[] = $objEc2->ec2Client->describeInstancesAsync();
                    }
                    $ec2Info = $this->resolvePromises($promises);

                    if (!empty($ec2Info)) {
                        foreach ($ec2Info as $info) {
                            $regionEc2Info = $info->get('Reservations');

                            if (!empty($regionEc2Info)) {
                                $args = [
                                    'region' => substr($regionEc2Info[0]['Instances'][0]['Placement']['AvailabilityZone'], 0, -1),
                                    'profile' => $account['profile'],
                                    'version' => $account['version'],
                                ];
                                $objCloudWatch = new CloudWatchService($args);

                                foreach ($regionEc2Info as $key => $singleEc2Info) {
                                    foreach ($singleEc2Info['Instances'] as $singleNode) {

                                        $instanceIds[] = $singleNode['InstanceId'];
                                        $singleNode['aws_account'] = $account['profile'];
                                        //$final_result[] = $singleNode;
                                        $final_result[$count]['InstanceId'] = $singleNode['InstanceId'];

                                        $final_result[$count]['Name'] = $singleNode['Tags'][0]['Value'];
                                        $final_result[$count]['InstanceType'] = $singleNode['InstanceType'];
                                        $final_result[$count]['PublicIpAddress'] = $singleNode['PublicIpAddress'];
                                        $final_result[$count]['Region'] = $singleNode['Placement']['AvailabilityZone'];
                                        $final_result[$count]['Status'] = $singleNode['State']['Name'];
                                        $final_result[$count]['aws_account'] = $singleNode['aws_account'];

                                        $matricsDimensions = [
                                            [
                                                'Name' => 'InstanceId',
                                                'Value' => $singleNode['InstanceId']
                                            ]
                                        ];
                                        $promises2[] = $objCloudWatch->prepareMatricStatisticPromise($matricsDimensions, 'AWS/EC2');
                                        $count++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $resultsMetricStatistics = $this->resolvePromises($promises2);

            if (!empty($resultsMetricStatistics)) {
                $json = file_get_contents('aws-monotoring-settings.json');
                $data = json_decode($json, TRUE);
                foreach ($resultsMetricStatistics as $key => $response) {
                    $dataPoints = $response['Datapoints'];
                    $cpuUtilization = $this->maxValueInArray($dataPoints, 'Average');
                    $final_result[$key]['cpuUtilization'] = $cpuUtilization;

                    //add alert condition
                    if ($cpuUtilization > $this->cpuThreshold) {

                        if (!empty($data['' . $final_result[$key]['InstanceId'] . 'lastEmailTimestamp']) && $data['' . $final_result[$key]['InstanceId'] . 'lastEmailTimestamp'] > strtotime('now')) {
                            continue;
                        }

                        $body = "Name: " . $final_result[$key]['Name'] . " InstanceId: " . $final_result[$key]['InstanceId'] . " CPU Utilization: " . $cpuUtilization . "";
                        $this->sendAlert($body);
                        $this->updateLastEmailTimestamp($final_result[$key]['InstanceId']);
                    }
                }
            }
            return $final_result;
        }
    }

    public function getRdsData()
    {
        if (!empty($this->accounts)) {

            $promises2 = array();

            $instanceIds = array();
            $final_result = array();

            $count=0;

            foreach ($this->accounts as $account) {

                if (!empty($account['regions'])) {

                    $promises = array();

                    foreach ($account['regions'] as $region) {
                        $args = [
                            'region' => $region,
                            'profile' => $account['profile'],
                            'version' => $account['version'],
                        ];
                        $objRds = new RdsService($args);
                        $promises[] = $objRds->rdsClient->describeDBInstancesAsync();
                    }
                    $rdsInfo = $this->resolvePromises($promises);

                    if (!empty($rdsInfo)) {
                        foreach ($rdsInfo as $info) {
                            $regionRdsInfo = $info->get('DBInstances');

                            if (!empty($regionRdsInfo)) {
                                $args = [
                                    'region' => substr($regionRdsInfo[0]['AvailabilityZone'], 0, -1),
                                    'profile' => $account['profile'],
                                    'version' => $account['version'],
                                ];
                                $objCloudWatch = new CloudWatchService($args);

                                foreach ($regionRdsInfo as $key => $singleRdsInfo) {

                                    $instanceIds[] = $singleRdsInfo['DBInstanceIdentifier'];
                                    $singleRdsInfo['aws_account'] = $account['profile'];
                                    //$final_result[] = $singleRdsInfo;
                                    $final_result[$count]['DBInstanceIdentifier'] = $singleRdsInfo['DBInstanceIdentifier'];

                                    $final_result[$count]['DBInstanceClass'] = $singleRdsInfo['DBInstanceClass'];
                                    $final_result[$count]['Engine'] = $singleRdsInfo['Engine'];
                                    $final_result[$count]['DBInstanceStatus'] = $singleRdsInfo['DBInstanceStatus'];
                                    $final_result[$count]['AllocatedStorage'] = $singleRdsInfo['AllocatedStorage'];
                                    $final_result[$count]['AvailabilityZone'] = $singleRdsInfo['AvailabilityZone'];
                                    $final_result[$count]['aws_account'] = $singleRdsInfo['aws_account'];

                                    $matricsDimensions = [
                                        [
                                            'Name' => 'DBInstanceIdentifier',
                                            'Value' => $singleRdsInfo['DBInstanceIdentifier']
                                        ]
                                    ];

                                    $promises2[] = $objCloudWatch->prepareMatricStatisticPromise($matricsDimensions, 'AWS/RDS');
                                    $count++;
                                }
                            }
                        }
                    }
                }
            }

            $resultsMetricStatistics = $this->resolvePromises($promises2);
            if (!empty($resultsMetricStatistics)) {
                $json = file_get_contents('aws-monotoring-settings.json');
                $data = json_decode($json, TRUE);
                foreach ($resultsMetricStatistics as $key => $response) {
                    $dataPoints = $response['Datapoints'];
                    $cpuUtilization = $this->maxValueInArray($dataPoints, 'Average');
                    $final_result[$key]['cpuUtilization'] = $cpuUtilization;

                    //add alert condition
                    if ($cpuUtilization > $this->cpuThreshold) {

                        if (!empty($data['' . $final_result[$key]['DBInstanceIdentifier'] . 'lastEmailTimestamp']) && $data['' . $final_result[$key]['DBInstanceIdentifier'] . 'lastEmailTimestamp'] > strtotime('now')) {
                            continue;
                        }

                        $body = "DBInstanceIdentifier: " . $final_result[$key]['DBInstanceIdentifier'] . " CPU Utilization: " . $cpuUtilization . "";
                        $this->sendAlert($body);
                        $this->updateLastEmailTimestamp($final_result[$key]['DBInstanceIdentifier']);
                    }
                }
            }

            return $final_result;
        }
    }

    public function sendAlert($body)
    {
        $promise = new Promise\Promise();
        $promise
            ->then(function ($value) {
                $postData = array(
                    "body" => $value,
                    "receivers" => $this->alertReceivers
                );

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $this->emailApi);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_exec($ch);
                curl_close($ch);

                echo "mail sent".PHP_EOL;

            });
        $promise->resolve($body);
    }

    private function maxValueInArray($array, $keyToSearch)
    {
        if (empty($array)) {
            return null;
        }
        $currentMax = NULL;
        foreach ($array as $arr) {
            foreach ($arr as $key => $value) {
                if ($key == $keyToSearch && ($value >= $currentMax)) {
                    $currentMax = $value;
                }
            }
        }

        return round($currentMax, 2);
    }

    public function resolvePromises($promises)
    {
        return Promise\all($promises)->then(function ($responses) {
            return $responses;
        })->wait();
    }

    private function updateLastEmailTimestamp($instanceId)
    {
        $promise = new Promise\Promise();
        $promise
            ->then(function ($value) {
                $json = file_get_contents('aws-monotoring-settings.json');
                $data = json_decode($json, TRUE);
                if (!empty($data)) {
                    $data['' . $value . 'lastEmailTimestamp'] = strtotime("+30 minutes");

                    $newJsonString = json_encode($data);
                    file_put_contents('aws-monotoring-settings.json', $newJsonString);
                }

                echo "timestamp updated" . PHP_EOL;

            });
        $promise->resolve($instanceId);
    }
}
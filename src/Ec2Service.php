<?php
namespace MyApp;
use Aws\Ec2\Ec2Client;

class Ec2Service
{
    public $ec2Client;

    public function __construct($args)
    {
        $this->ec2Client = new Ec2Client($args);
    }
}
<?php
namespace MyApp;
use Aws\Rds\RdsClient;

class RdsService
{
    public $rdsClient;

    public function __construct($args)
    {
        $this->rdsClient = new RdsClient($args);
    }

    public function describeDbInstances(){
        return $this->rdsClient->describeDBInstancesAsync();
    }
}
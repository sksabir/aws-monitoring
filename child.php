<?php
require_once __DIR__ . '/vendor/autoload.php';

use MyApp\AwsMonitoring;

$obj = new AwsMonitoring();
$data = $obj->getData();
echo json_encode(array("ec2" => $data));
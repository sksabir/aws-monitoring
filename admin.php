<?php
if(isset($_POST['monitoring_setting_submit']) && $_POST['monitoring_setting_submit'] == 'Submit'){

    unset($_POST['monitoring_setting_submit']);
    $inputs=$_POST;
    $newJsonString = json_encode($inputs);
    file_put_contents('aws-monotoring-settings.json', $newJsonString);
}

$values = file_get_contents('aws-monotoring-settings.json');
$email_ids = '';
$cpu_threshold = '';
if(!empty($values)){
    $values = json_decode($values);

    if(!empty($values)){
        $email_ids = $values->email_ids;
        $cpu_threshold = $values->cpu_threshold;
    }
}

?>
<!DOCTYPE html>
<html>
<style>
    input[type=text]{
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }

    input[type=number]{
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }

    input[type=submit] {
        width: 100%;
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

    input[type=submit]:hover {
        background-color: #45a049;
    }

    div {
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px;
        width: 600px;
    }
</style>
<body>

<h3>AWS Monitoring Settings</h3>

<div>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <label for="email-ids">Email Ids (Comma Separated)</label>
        <input type="text" id="email-ids" name="email_ids" placeholder="emample: john@email1.com,doe@email2.com" value="<?php echo $email_ids; ?>" required>

        <label for="cpu-threshold">CPU Threshold</label>
        <input type="number" id="cpu-threshold" name="cpu_threshold" placeholder="example: 60" value="<?php echo $cpu_threshold; ?>" required>

        <input type="submit" name="monitoring_setting_submit" value="Submit">
    </form>
</div>

</body>
</html>

new Vue({
    el: '#aws-matrices',

    data: {
        ws: null,
        flag: 'Waiting for socket connection',
        sortKey: 'cpuUtilization',
        reverse: true,
        search: '',
        columns: ['InstanceId', 'Name', 'cpuUtilization', 'InstanceType', 'PublicIpAddress', 'Region', 'Status', 'aws_account'],
        matrices: []
    },

    methods: {
        sortBy: function (sortKey) {
            this.reverse = (this.sortKey == sortKey) ? !this.reverse : false;
            this.sortKey = sortKey;
        }
    },

    created: function () {
        this.ws = new WebSocket(SOCKET_URL);
        this.ws.addEventListener('open', (e) => {
            this.flag = "Connection established! Please wait for incoming messages";
        });

        this.ws.addEventListener('message', (e) => {
            var socketdatajson = JSON.parse(e.data);
            console.log(socketdatajson);
            if(socketdatajson.ec2){
                this.matrices = socketdatajson.ec2;
                this.flag = "Data last updated on " + Date();
            }
        });

        this.ws.addEventListener('close', (e) => {
            this.flag = "Connection closed! Reloading...";
            location.reload();
        });
    }
});
